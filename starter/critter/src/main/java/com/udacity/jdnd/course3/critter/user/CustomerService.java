package com.udacity.jdnd.course3.critter.user;

import com.udacity.jdnd.course3.critter.pet.Pet;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CustomerService {
    final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer saveCustomer(Customer customer) {
        return  customerRepository.save(customer);
    }

    public List<Customer> getALL() {
        return customerRepository.findAll();
    }

    public Customer getById(long ownerId) {
        return customerRepository.getOne(ownerId);
    }

    public void addPetToCustomer(Pet pet, Customer customer) {
        List<Pet>  pets = customer.getPets();
        if (pets == null) {
            pets = new ArrayList<>();
        }
        pets.add(pet);
        customer.setPets(pets);
    }

    public Customer getForPet(long petId) {
        return customerRepository.findByPetsId(petId);
    }
}

package com.udacity.jdnd.course3.critter.pet;

import com.udacity.jdnd.course3.critter.user.CustomerService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PetService {
    final PetRepository petRepository;
    final CustomerService customerService;

    public PetService(PetRepository petRepository, CustomerService customerService) {
        this.petRepository = petRepository;
        this.customerService = customerService;
    }

    public Pet save(Pet pet) {

        Pet savedPet = petRepository.save(pet);

        if (pet.getCustomer() != null) {
            customerService.addPetToCustomer(savedPet, savedPet.getCustomer());
        }
        return savedPet;
    }

    public Pet findById(long petId) {
        return petRepository.getOne(petId);
    }

    public List<Pet> findByOwner(Long ownerId) {
        List<Pet> pets = petRepository.findAllByCustomerId(ownerId);
        return pets;
    }

    public List<Pet> all() {
        return petRepository.findAll();
    }
}

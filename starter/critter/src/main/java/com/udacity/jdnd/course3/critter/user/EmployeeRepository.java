package com.udacity.jdnd.course3.critter.user;

import com.udacity.jdnd.course3.critter.user.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    /***
     *  solution from stackoverflow https://stackoverflow.com/a/21645545/5914469
     *  counts the skills of the employee that are skills in the accepted set of skills,
     *  and if the number of such skills is eaqual to the size of the accepted set of skills
     *  (meaning that all skills in the set are skills of the employee), returns the employee.
     * @param skills set of skills to match
     * @param count size of the set
     * @param dayOfWeek day the employee should be available
     * @return List of employees that match both criteria
     */
    @Query("select e from Employee e where :count = " +
            "(select count(distinct skill) from Employee e2 " +
            "inner join e2.skills skill where skill in :skills and e = e2) " +
            "and :dayOfWeek member of e.daysAvailable")

    List<Employee> findAvailable(Set<EmployeeSkill> skills, Long count, DayOfWeek dayOfWeek);

}


package com.udacity.jdnd.course3.critter.schedule;

import com.udacity.jdnd.course3.critter.pet.Pet;
import com.udacity.jdnd.course3.critter.pet.PetService;
import com.udacity.jdnd.course3.critter.user.Employee;
import com.udacity.jdnd.course3.critter.user.EmployeeService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ScheduleService {
    final ScheduleRepository scheduleRepository;
    final EmployeeService employeeService;
    final PetService petService;


    public ScheduleService(ScheduleRepository scheduleRepository, EmployeeService employeeService, PetService petService) {
        this.scheduleRepository = scheduleRepository;
        this.employeeService = employeeService;
        this.petService = petService;
    }

    public List<Schedule> getScheduleForEmployee(long employeeId) {
        Employee employee = employeeService.findById(employeeId);
        return scheduleRepository.findAllByEmployees(employee);
    }

    public Schedule save(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    public List<Schedule> getScheduleForPet(long petId) {
        Pet pet = petService.findById(petId);
        return scheduleRepository.findAllByPets(pet);
    }

    public List<Schedule> getAll() {
        return scheduleRepository.findAll();
    }
}
